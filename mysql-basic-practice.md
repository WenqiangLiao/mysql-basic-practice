#### Table-Practice

1. WorkBench 连接MySQL库:

   

2. 表相关练习

   - 创建表

     ``` mysql
     // 创建表student
     use student_manage_sys_liaowenqiang;
     
     create table `student_manage_sys_liaowenqiang`.`student` (
     `id` int not null auto_increment primary key,
     `name` varchar(100) not null,
     `age` int unsigned not null,
     `sex` char not null,
     `phone` char not null
     );
     
     // 创建表teacher
     create table teacher (
     `id` int not null auto_increment primary key,
     `name` varchar(100) not null,
     `period` int not null,
     `description` text
     );
     
     // 创建表exam
     create table exam (
     `id` int not null auto_increment primary key,
     `name` varchar(100) not null,
     `subject_id` int not null
     );
     
     // 创建表score
     create table score (
     `id` int not null auto_increment primary key,
     `exam_id` int not null,
     `student_id` int not null,
     `score` float not null
     )
     ```

- 操作表

  - 老师表(teacher)里面添加练习方式(phone),类型为CHAR，默认为11位

    `  alter table teacher add phone char(11) not null;`

  - 成绩表里面分数添加检查，只能大于等于0，小于等于100

    `alter table score add constraint check (score >= 0 and score <= 100);`

  - 修改老师表和学生表里姓名字段的名称为gender

    ``` 
    alter table student 
    	change column name gender
    		varchar(100) not null;
    		
    alter table teacher 
    	change column name gender
    		varchar(100) not null;
    ```

  -  删除成绩表

    ` drop table score;`

  - 重新创建成绩表，字段类型与上面保持一致

    ``` mysql
    create table score (
    `id` int not null auto_increment primary key,
    `exam_id` int not null,
    `student_id` int not null,
    `score` float not null
    );
    ```



#### Data-Operation

1. 新增数据

   ``` mysql
   insert into student 
   	values
   		(1, "张三", 18, "男", "15342349123"),
           (2, "李四", 20, "女", "13884356789"),
           (3, "王五", 26, "女", "13884359789"),
           (4, "赵六", 16, "女", "13884334789");
   ```

   ![](https://icon.qiantucdn.com/20200403/224dabc79a9f7f6ead8f60b247ec1ac62)

   ``` mysql
   insert into teacher
   	values
   		(1, "刘老师", 38, "男", "15343349123", 1),
           (2, "李老师", 40, "女", "13885356789", 2),
           (3, "张老师", 56, "女", "18784359789", 3);
   ```

   ![](https://icon.qiantucdn.com/20200403/749fcc1dbdbe891ba0cc32d2e5597d562)

   ```mysql
   insert into subject
   	values
   		(1, "语文", 40, ""),
           (2, "数学", 30, ""),
           (3, "英语", 45, "");
   ```

   ![](https://icon.qiantucdn.com/20200403/948be3f1b24f567dba42c0dff8e11e932)

   ```mysql
   insert into exam
   	values
   		(1, "期末考试", 1),
           (2, "期末考试", 2),
           (3, "期中考试", 3),
           (4, "期中考试", 4);
   ```

   ![](https://icon.qiantucdn.com/20200403/df619320aa61a7e16c5889805715e1a62)

   ```mysql
   insert into score (student_id, exam_id, score)
   	values
   		(1, 1, 80),
           (1,2,60),
           (1,3,70),
           (1,4,60.5),
           (2,1,40),
           (2,2,30),
           (2,3,20),
           (2,4,55),
           (3,1,48),
           (3,2,39),
           (3,3,70),
           (3,4,95),
           (4,1,78),
           (4,2,88),
           (4,3,0),
           (4,4,100);
   ```

   ![](https://icon.qiantucdn.com/20200403/bbface86b78da8705054338e998e9e082)

2. 修改数据

   - 修改学生赵六的名字的联系方式为：13456789876

     `update student set phone="13456789876" where gender="赵六";`

     ![](https://icon.qiantucdn.com/20200403/a884930852e6e289e655ff508e1d63072)

   - 给语文这个科目添加描述：该课程是本校特殊课程，由非常资深的刘老师授课

     ```mysql
     update subject set description="该课程是本校特殊课程,由非常资深的刘老师授课。" 
     	where name="语文";
     ```

     ![](https://icon.qiantucdn.com/20200403/5ee05efd6b6f23c2cea419c5572f4e862)

   - 修改赵六同学语文中期考试的成绩为98分

     ```mysql
     update score 
     set score=98 
     where id in (select * from (select id 
     from score where score.student_id=(
     	select id from student where gender="赵六") 
     and score.exam_id=(select id from exam 
     	where name="期中考试" 
         and subject_id=(select id from subject 
     		where name="语文")
     ))tempRes);
     ```

     ![](https://icon.qiantucdn.com/20200403/293084c68b6810293326e20fdc4843192)

     

3. 查询数据

   - 查询所有学生

     ` select gender as all_student from student;`

     ![](https://icon.qiantucdn.com/20200403/cf8a5334488e3d713400891b4440b51d2)

     

   - 查询所有成年学生(年龄大于等于18岁)

     `select gender as adult_student, age from student where age >= 18;`

     ![](https://icon.qiantucdn.com/20200403/4621f92735e4d70ed0e4d30e53b19c5e2)

4. 删除数据

   - 由于赵六同学在语文中期考试中作弊，于是需要在成绩表中删除该记录

     ```mysql
     delete from score where id in 
     (select * from (select id 
     from score where score.student_id=(
     	select id from student where gender="赵六") 
     and score.exam_id=(select id from exam 
     	where name="期中考试" 
         and subject_id=(select id from subject 
     		where name="语文")
     ))tempRes);
     ```

     ![](https://icon.qiantucdn.com/20200403/3d575d53c97db8b17b83e232b8f841932)

     



#### 高级查询

1. 查询语文期末考试的所有成绩。

   ```mysql
   select t1.name, t2.name, t3.score from subject t1 
   inner join exam t2 on t1.id=t2.subject_id 
   inner join score t3 on t3.exam_id=t2.id
   where t1.name="语文" and t2.name="期末考试";
   ```

   ![](https://icon.qiantucdn.com/20200403/f8975a894dadbeb96c22c2cca76368bb2)

   

2. 查询参加语文中期考试的所有学生。

   ```mysql
   select t4.gender, t4.age, t4.sex, t4.phone from subject t1
   inner join exam t2 on t1.id=t2.subject_id 
   inner join score t3 on t3.exam_id=t2.id
   inner join student t4 on t4.id=t3.student_id
   where t1.name="语文" and t2.name="期中考试";
   ```

   ![](https://icon.qiantucdn.com/20200403/e85a1a7243854dfe95f7d48c7a7c2e7b2)

   

3. 查询参加语文期末考试及格的学生(分数大于等于60)。

   ```mysql
   select t4.gender, t4.age, t4.sex, t4.phone from subject t1
   inner join exam t2 on t1.id=t2.subject_id 
   inner join score t3 on t3.exam_id=t2.id
   inner join student t4 on t4.id=t3.student_id
   where t1.name="语文" and t2.name="期末考试" and t3.score>=60;
   ```

   ![](https://icon.qiantucdn.com/20200403/db0a9b96eab1b1ef8d7f08b9b5a0547f2)

   

4. 查询参加李老师所带科目期末考试及格的学生(分数大于等于60)。

   ```mysql
   select t5.gender, t5.age, t5.sex, t5.phone from teacher t1
   inner join subject t2 on t1.subject_id=t2.id
   inner join exam t3 on t3.subject_id=t2.id
   inner join score t4 on t4.exam_id=t3.id
   inner join student t5 on t4.student_id=t5.id
   where t1.gender="李老师" and t3.name="期末考试" and t4.score>=60;
   ```

   ![](https://icon.qiantucdn.com/20200403/e570d45f5465be4c30f12ac76174891a2)

   

5. 查询参加李老师所带科目期末考试的平均成绩。

   ```mysql
   select avg(t4.score) as avg_score from teacher t1
   inner join subject t2 on t1.subject_id=t2.id
   inner join exam t3 on t3.subject_id=t2.id
   inner join score t4 on t4.exam_id=t3.id
   inner join student t5 on t4.student_id=t5.id
   where t1.gender="李老师" and t3.name="期末考试";
   ```

   ![](https://icon.qiantucdn.com/20200403/c796806e5370344d07156c4f569492992)

   

6. 查询参加李老师所带科目所有考试的最高分。

   ```mysql
   select t4.score from teacher t1
   inner join subject t2 on t1.subject_id=t2.id
   inner join exam t3 on t3.subject_id=t2.id
   inner join score t4 on t4.exam_id=t3.id
   inner join student t5 on t4.student_id=t5.id
   where t1.gender="李老师" and t3.name="期末考试"
   order by score desc limit 1;
   ```

   ![](https://icon.qiantucdn.com/20200403/cefee1f1be8ba6754c82480316d9440b2)

   

7. 查询参加李老师所带科目所有考试的最低分。

   ```mysql
   select t4.score as lowest_score from teacher t1
   inner join subject t2 on t1.subject_id=t2.id
   inner join exam t3 on t3.subject_id=t2.id
   inner join score t4 on t4.exam_id=t3.id
   inner join student t5 on t4.student_id=t5.id
   where t1.gender="李老师" and t3.name="期末考试"
   order by score asc limit 1;
   ```

   ![](https://icon.qiantucdn.com/20200403/734a5adef49b1f968db520356e13304f2)

8. 查询张三同学期末考试所有科目的总分。

   ```mysql
   select sum(t2.score) as total_score from  student t1
   inner join score t2 on t2.student_id=t1.id
   inner join exam t3 on t3.id=t2.exam_id
   where t1.gender="张三" and t3.name="期末考试";
   ```

   ![](https://icon.qiantucdn.com/20200403/78814d4515bf804984bc19deacf71e772)

   

9. 更新张三同学期末考试语文的分数为88分。

   ```mysql
   update score set score=88
   where id in (select * from (
   select t2.id from  student t1
   inner join score t2 on t2.student_id=t1.id
   inner join exam t3 on t3.id=t2.exam_id
   inner join subject t4 on t3.subject_id=t4.id
   where t1.gender="张三" and t3.name="期末考试" and t4.name="语文")temp);
   ```

   ![](https://icon.qiantucdn.com/20200403/d56dce9e639021a7b96722f9ada4adc92)

   

10. 查询参加中期考试的学生人数。

    ```mysql
    select count(distinct t2.student_id) as num_student_final_exam from exam t1
    inner join score t2 on t1.id=t2.exam_id
    where t1.name="期中考试";
    ```

    ![](https://icon.qiantucdn.com/20200403/d09c1bb3f1b47f6169ddc6a26e6e86482)

    